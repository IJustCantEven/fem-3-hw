const screenWidth = document.querySelector('.dvw');
screenWidth.innerHTML = `Device width ${window.innerWidth}px`;

window.addEventListener('resize',() => {
  screenWidth.innerHTML = `Device width ${window.innerWidth}px`;
});