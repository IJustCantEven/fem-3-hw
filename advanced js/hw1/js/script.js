var availableSizes = {
  SIZE_SMALL: 'small',
  SIZE_LARGE: 'large'
};

var sizeProperties = {
  SIZE_SMALL: {
    price: '50',
    calories: '200'
  },
  SIZE_LARGE: {
    price: '100',
    calories:'400'
  }
};

var availableStuffings = {
  STUFFING_CHEESE: 'cheese',
  STUFFING_SALAD: 'salad',
  STUFFING_POTATO: 'potato',
  STUFFING_MAYO: 'mayo',
  STUFFING_SPICE: 'spice'
};

var stuffingProperties = {
  STUFFING_CHEESE: {
    price: '10',
    calories: '400'
  },
  STUFFING_SALAD:{
    price: '30',
    calories: '30'
  },
  STUFFING_POTATO: {
    price: '20',
    calories: '50'
  },
  STUFFING_MAYO: {
    price: '30',
    calories: '20'
  },
  STUFFING_SPICE:{
    price: '10',
    calories: '30'
  }
};

var availableToppings = {
  TOPPING_MAYO: 'mayo',
  TOPPING_SPICE: 'spice'
};

var toppingProperties = {
  TOPPING_MAYO: {
    price: '5',
    calories: '30'
  },
  TOPPING_SPICE: {
    price: '5',
    calories: '0'
  }
};


var size = String(prompt('Please enter the size')).toLowerCase();
var mainStuffing = String(prompt("Please enter your main stuffing")).toLowerCase();


function Hamburger(size, stuffing) {
  function inArray(needle,haystack)
  {
    var count=haystack.length;
    for(var i=0;i<count;i++)
    {
      if(haystack[i]===needle){return true;}
    }
    return false;
  }

  function getKeyByValue(object, value) {
    return Object.keys(object).find(key => object[key] === value);
  }

  this.size = getKeyByValue(availableSizes,size);
  if (this.size === undefined){
    throw new SyntaxError('Invalid burger size');
  }

  this.stuffing = getKeyByValue(availableStuffings,stuffing);
  if (this.stuffing === undefined){
    throw  new SyntaxError('Invalid burger stuffing');
  }

  var price = Number(sizeProperties[this.size].price) + Number(stuffingProperties[this.stuffing].price);
  var calories = Number(sizeProperties[this.size].calories) + Number(stuffingProperties[this.stuffing].calories);

  this.getPrice = function () {
    return price + ' гривен';
  };

  this.getCalories = function () {
    return calories + ' калорий'
  };

  var additionalStuffings = [];
  var toppings = [];

  try {


    this.addStuffing = function (stuffing) {
      if (getKeyByValue(availableStuffings, stuffing) === undefined){
        throw new SyntaxError('Invalid additional stuffing')
      }
      else if ((inArray(stuffing,additionalStuffings) === true) || this.stuffing === getKeyByValue(availableStuffings,stuffing)){
        throw new SyntaxError('This stuffing already exists');
      }
      price += Number(stuffingProperties[getKeyByValue(availableStuffings,stuffing)].price);
      calories += Number(stuffingProperties[getKeyByValue(availableStuffings,stuffing)].calories);
      additionalStuffings.push(stuffing);
    };

    this.removeStuffing = function (stuffing) {
      if (getKeyByValue(availableStuffings,stuffing) === undefined){
        throw new SyntaxError('Invalid stuffing to remove');
      }

      else if (getKeyByValue(availableStuffings,stuffing) === this.stuffing){
        throw new SyntaxError('Cant remove main stuffing');
      }

      else if ((inArray(stuffing,additionalStuffings) === false)){
        throw new SyntaxError('No stuffing to remove');
      }

      price -= Number(stuffingProperties[getKeyByValue(availableStuffings,stuffing)].price);
      calories -= Number(stuffingProperties[getKeyByValue(availableStuffings,stuffing)].calories);

      additionalStuffings.splice(additionalStuffings.indexOf(stuffing),1);
    };

    this.addTopping = function (topping) {
      if (getKeyByValue(availableToppings,topping) === undefined){
        throw new SyntaxError('Invalid topping');
      }
      else if(inArray(topping, toppings) === true){
        throw new SyntaxError('This topping already exists');
      }

      price += Number(toppingProperties[getKeyByValue(availableToppings,topping)].price);
      calories += Number(toppingProperties[getKeyByValue(availableToppings,topping)].calories);
      toppings.push(topping);
    };

    this.removeTopping = function (topping) {
      if (getKeyByValue(availableToppings,topping) === undefined){
        throw new SyntaxError('Invalid topping');
      }
      else if(inArray(topping, toppings) === false){
        throw new SyntaxError('Nothing to remove');
      }

      price -= Number(toppingProperties[getKeyByValue(availableToppings,topping)].price);
      calories -= Number(toppingProperties[getKeyByValue(availableToppings,topping)].calories);
      toppings.splice(toppings.indexOf(topping),1);
    }

  } catch (err) {
    console.log(err);
  }

}


var burger = new Hamburger(size, mainStuffing);
