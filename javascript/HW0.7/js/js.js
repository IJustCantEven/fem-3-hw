let array = ['js', 'is', 'the', 'best', 'language', 'ever'];

function arrayOutput(array) {
  let list = document.createElement('ul');

  const arrayListItems = array.map(item => {
    let listItem = `<li> ${item} </li>`;
    return listItem;
  });
  list.innerHTML = arrayListItems.join(',');
  return list
}



document.querySelector('script').before(arrayOutput(array));