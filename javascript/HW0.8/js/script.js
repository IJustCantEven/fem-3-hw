const price = document.createElement('span');
const input = document.createElement('textarea'); //input.value = 'Enter'; input.innerHTML = 'Enter';
const cancel = document.createElement('div'); cancel.innerHTML = '<p>x</p>';
cancel.style.display = 'none';
const sorry = document.createElement('p'); sorry.innerHTML = "Sorry, wrong input";
sorry.style.diplay = 'none';
let fragment = document.createDocumentFragment();
fragment.appendChild(price);
fragment.appendChild(input);
fragment.appendChild(cancel);
fragment.appendChild(sorry);
document.querySelector('script').before(fragment);

input.onfocus = () => {
  price.style.color = 'black';
  input.style.backgroundColor = 'green';
  sorry.style.display = 'none';
};

input.onblur = () => {
  if ((Number(input.value) <= 0)) {
    input.style.backgroundColor = 'red';
    sorry.style.display = 'block';
  } else {
    price.style.color = 'green';
    price.innerHTML = `${Number(input.value)}`;
    cancel.style.display = 'inline-block';
  }
};

cancel.onclick = () => {
  cancel.style.display = 'none';
  price.innerHTML = '';
  input.value = '';
  input.style.backgroundColor = 'white';
};
