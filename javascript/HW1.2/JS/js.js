const buttons = document.createElement("div");
document.querySelector(".images-wrapper").after(buttons);
const stopBtn = document.createElement("button");
stopBtn.innerText = "Stop me!";
const resumeBtn = document.createElement("button");
resumeBtn.innerText = "Start me!";
const images = document.querySelectorAll(".images-wrapper .image-to-show");
let counter = 0;
let isPaused = false;

buttons.appendChild(stopBtn).after(resumeBtn);

buttons.addEventListener("click", event => {
  if (event.target === stopBtn) {
    isPaused = true
  } else if (event.target === resumeBtn) {
    isPaused = false
  }
});

setInterval(() => {
  if (!isPaused) {
    images.forEach(elem => {
      elem.hidden = true;
    });
    images[counter].hidden = false;
    if (counter === images.length - 1) {
      counter = 0
    } else {
      counter++
    }
  }
}, 1000);