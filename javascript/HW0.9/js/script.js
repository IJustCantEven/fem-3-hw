let tabs = document.querySelector('.tabs');
let tabsContent = document.querySelector('.tabs-content');

let tabsLength = tabs.children.length;

let tabsChildren = tabs.children;
let tabsContentChildren = tabsContent.children;

for(let counter = 0; counter < tabsLength; counter++){
  tabsChildren[counter].dataset.index = String(counter);
  if(counter !== 0){
    tabsContentChildren[counter].hidden = true;
  }
}

tabs.addEventListener("click",() => {
  tabs.querySelector('.active').classList.toggle('active');
  tabsContent.querySelector("li:not([hidden])").hidden = true;
  event.target.classList.toggle('active');
  tabsContentChildren[event.target.dataset.index].hidden = false;
});
