document.querySelector('.toggle').addEventListener('click',()=>{
   if(localStorage.getItem('status')==='not-active'){
       localStorage.setItem('status','active');
       document.body.classList.add('active-bg');
       document.querySelectorAll('.text').forEach( item => {
           item.classList.add('active-text');
       });
       document.querySelectorAll('.bg').forEach( item => {
           item.classList.add('active-bg');
       })
   } else{
       localStorage.setItem('status', 'not-active');
       document.body.classList.remove('active-bg');
       document.querySelectorAll('.text').forEach( item => {
           item.classList.remove('active-text');
       });
       document.querySelectorAll('.bg').forEach( item => {
           item.classList.remove('active-bg');
       })
   }
});

if (localStorage.getItem('status') === 'active') {
    document.body.classList.add('active-bg');
    document.querySelectorAll('.text').forEach( item => {
        item.classList.add('active-text');
    });
    document.querySelectorAll('.bg').forEach( item => {
        item.classList.add('active-bg');
    })
}
