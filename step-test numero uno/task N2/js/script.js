const CreditCard = {
    moneyAmount: 10000,
    pincode: 2222,
    numberOfTries: 3,
    status: 'active',
    getCash: function (pin, moneyAmount) {
        if (this.status === 'disabled') {
            alert('Sorry, your card has been disabled. Too many tries.')
        }

        else if (pin !== this.pincode && this.numberOfTries !== 0) {
            alert(`Sorry, wrong pin. Number of remaining tries = ${this.numberOfTries}`)
            this.numberOfTries -= 1;
        }

        else if (this.numberOfTries === 0) {
            this.status = 'disabled';
            alert('Oops! Too many tries! Your card has been blocked :(')
        }

        else if (this.pincode === pin) {
            if (moneyAmount <= this.moneyAmount) {
                this.moneyAmount -= moneyAmount;
                alert(`Here go your ${moneyAmount}. Money remaining: ${this.moneyAmount}`);
                this.numberOfTries = 3;
            } else {
                alert('Sorry, insufficient funds');
                this.numberOfTries = 3;
            }
        }

    }
};


document.querySelector('#get-cash').addEventListener('click', () => {
    let pin = + document.querySelector('#pin-code').value;
    let moneyAmount = document.querySelector('#money-sum').value;
    CreditCard.getCash(pin, moneyAmount);
});

