function isSpam(string, keyWord, repetition) {
    let result = false;
    if(string.split(' ').filter(elem => {return elem === `${keyWord}`}).length > 3){
        result = true
    }
    return result;
}

document.querySelector('#send-comment').addEventListener('click', () => {
    let comment = document.querySelector('#comment').value;
    let spamWord = document.querySelector('#spam-word').value;
    console.log(isSpam(comment, spamWord));
});
